$(function(){
    $('.remove').click( e => {
        e.preventDefault();
        var id = $(e.target).data('id'), type = $(e.target).data('type');

        $.ajax({type: 'DELETE', url: 'api/' + type + '/' + id});
        $(e.target).parents('tr').remove()
    })

    $('#add-employee').click( e => {
        e.preventDefault();
        var firstname = $('#firstname').val(), lastname = $('#lastname').val();

        $.post('api/employee', {
            firstname: firstname,
            lastname: lastname
        },
        function() {
            location.reload()
        });
    });
    
    $('#add-thing').click( e => {
        e.preventDefault();
        var name = $('#name').val();

        $.post('api/thing', {
            name: name,
        },
        function() {
            location.reload()
        });
    });
    
    $('.assing-person').on('change', e => {
        e.preventDefault();

        var activeId = $(e.target).val(),
            thingId = $(e.target).prop('title');
        
        $.ajax({
            type: 'PUT',
            url: 'api/thing/' + thingId + '/'+activeId
        })

    });
    
    $('.remove-thing').click(function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        
        $.ajax({type: 'PUT', url: 'api/thing/' + id});
        $(this).parents('.button').remove()
    })
})