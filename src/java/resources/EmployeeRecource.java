    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import ejb.DataBean;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import model.Employee;

/**
 *
 * @author rafal
 */
@Path("/employee")
public class EmployeeRecource {
    @EJB
    private DataBean db;
    
    @DELETE
    @Path("{id}")
    public void removeEmployee(@PathParam("id") int id) {
        try {
            db.removeEmployee(id);
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
        
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void newEmployee(@FormParam("firstname") String firstname, @FormParam("lastname") String lastname) {
        try {
            db.addEmployee(firstname, lastname);
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
}
