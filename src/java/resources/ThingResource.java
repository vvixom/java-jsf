/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import ejb.DataBean;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author rafal
 */

@Path("/thing")
public class ThingResource {
    @EJB
    private DataBean db;
    
    @DELETE
    @Path("{id}")
    public String removeThing(@PathParam("id") int id) {
        try {
            db.removeThing(id);
        } catch(Exception e) {}
        
        return "{status: 'ok'}";
    }
    
    @PUT
    @Path("{id}")
    public void removeEmployeeFromThing(@PathParam("id") int id) {
        try {
            db.removeEmployeeFromThing(id);
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void newThing(@FormParam("name") String name) {
        try {
            db.addThing(name);
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    @PUT
    @Path("{id}/{employeeId}")
    public void assingPerson(@PathParam("id") int id, @PathParam("employeeId") int employeeId) {
        try {
            db.assingPerson(id, employeeId);
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
}
