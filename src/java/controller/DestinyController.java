package controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ejb.DataBean;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Destiny;
import model.DestinyThing;
import model.Thing;
import org.jboss.logging.Logger;

/**
 *
 * @author rafal
 */
@Named(value = "destinyController")
@RequestScoped
public class DestinyController implements Serializable {
    @EJB
    private DataBean db;
    
    private List<Destiny> destines;
    private String name;

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /**
     * Creates a new instance of DestinyController
     */
    public DestinyController() {
    }
    
    public List<Destiny> getDestines() {
        try {
            this.destines = db.getDestines();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return destines;
    }
    
    public void createDestiny() {
        try {
            db.addDestiny(name);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
}
