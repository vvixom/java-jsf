/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.DataBean;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import model.Employee;
import model.Thing;
import org.jboss.logging.Logger;

/**
 *
 * @author rafal
 */
@ManagedBean(name = "employeesController")
@ViewScoped
public class EmployeesController implements Serializable {
    @EJB
    private DataBean db;
    
    private static Logger log = Logger.getLogger("BEAN");

    private List<Employee> employed;
    private List<Thing> things;
    
    private String selectedThing;

    public void setSelectedThing(String selectedThing) {
        this.selectedThing = selectedThing;
    }

    public String getSelectedThing() {
        return selectedThing;
    }

    private String firstname;
    private String lastname;
    private int id;
    
    /**
     * Creates a new instance of EmployeesController
     */
    public EmployeesController() {
    }
    
    public List<Employee> getEmployed() {
        try {
            this.employed = db.getEmployees();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return employed;
    }
    
    public List<Thing> getThings() {
        try {
            this.things = db.getThings();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return things;
    }
    
    public String getFirstname() {
        return this.firstname;
    }
    
    public String getLastname() {
        return this.lastname;
    }
    
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    public void createEmployee() {
        try {
            db.addEmployee(firstname, lastname);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public void removeEmployee(int id) {
        try {
            db.removeEmployee(id);
        } catch(Exception e) {}
    }
    
}
