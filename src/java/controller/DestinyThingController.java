package controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ejb.DataBean;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.Destiny;
import model.DestinyThing;
import model.Thing;
import org.jboss.logging.Logger;

/**
 *
 * @author rafal
 */
@Named(value = "destinyThingController")
@RequestScoped
public class DestinyThingController implements Serializable {
    @EJB
    private DataBean db;
    
    private List<Destiny> destines;
    private List<Thing> things;
    
    private int destinyValue;
    private int thingValue;

    
    public int getDestinyValue() {
        return destinyValue;
    }
   
    public void setDestinyValue(int destinyValue) {
        this.destinyValue = destinyValue;
    }
    
    public int getThingValue() {
        return thingValue;
    }
   
    public void setThingValue(int thingValue) {
        this.thingValue = thingValue;
    }


    /**
     * Creates a new instance of DestinyController
     */
    public DestinyThingController() {
    }
    
    public List<Destiny> getDestines() {
        try {
            this.destines = db.getDestines();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return destines;
    }
    
    public List<Thing> getThings() {
        try {
            this.things = db.getThings();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return things;
    }
    
    public void create() {
        try {
            db.addDestinyThing(thingValue, destinyValue);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
}
