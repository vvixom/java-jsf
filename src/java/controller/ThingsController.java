/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import ejb.DataBean;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import model.Employee;
import model.Thing;

/**
 *
 * @author rafal
 */

@ManagedBean(name = "thingsController")
@RequestScoped
public class ThingsController implements Serializable {
    @EJB
    private DataBean db;
    
    private List<Employee> employed;
    private List<Thing> things;
    private String name;

    private int id;
    
    private String selectedPerson;

    public void setSelectedPerson(String selectedPerson) {
        this.selectedPerson = selectedPerson;
    }

    public String getSelectedPerson() {
        return selectedPerson;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public List<Employee> getEmployed() {
        try {
            this.employed = db.getEmployees();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return employed;
    }
    
    public List<Thing> getThings() {
        try {
            this.things = db.getThings();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return things;
    }
    
    public void createThing() {
        try {
            db.addThing(name);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
