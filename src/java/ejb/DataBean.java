/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import com.sun.istack.logging.Logger;
import java.util.List;
import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import model.Destiny;
import model.DestinyThing;
import model.Employee;
import model.Thing;

/**
 *
 * @author rafal
 */
@Stateful
public class DataBean {
    
    @PersistenceContext
    private EntityManager em;    
    public List<Employee> getEmployees() {
        List<Employee> employees = null;
        
        try {
            employees = (List<Employee>) em.createNamedQuery("Employee.findAll").getResultList();
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
        return employees;
    }
    
    public void addEmployee(String firstname, String lastname) {
        try {
            Employee newGuy = new Employee(Integer.valueOf(1), firstname, lastname);
            em.persist(newGuy);
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    public void removeEmployee(int id) {
        Employee item = null;
        
        try {
            item = (Employee) em.find(Employee.class, id);
            em.remove(item);
        } catch (NoResultException e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    public List<Thing> getThings() {
        List<Thing> things = null;
        
        try {
            things = (List<Thing>) em.createNamedQuery("Thing.findAll").getResultList();
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
        return things;
    }
    
    public void addThing(String name) {
        try {
            Thing newThing = new Thing(Integer.valueOf(1), name);
            em.persist(newThing);
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    public void addDestiny(String name) {
        try {
            Destiny newDestiny = new Destiny(Integer.valueOf(1), name);
            em.persist(newDestiny);
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    public void removeThing(int id) {
        Thing item = null;
        
        try {
            item = (Thing) em.find(Thing.class, id);
            em.remove(item);
            em.getEntityManagerFactory().getCache().evictAll();
        } catch (NoResultException e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    public void removeEmployeeFromThing(int id) {
        Thing item = null;
        
        try {
            item = (Thing) em.find(Thing.class, id);
            item.setIdEmployee(null);
            em.getEntityManagerFactory().getCache().evictAll();
        } catch (NoResultException e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    public void assingPerson(int id, int employeeId) {
        Thing item = null;

        try {
            item = (Thing) em.find(Thing.class, id);
            item.setIdEmployee(employeeId);
            em.getEntityManagerFactory().getCache().evictAll();
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
    }
    
    public List<Destiny> getDestines() {
        List<Destiny> destines = null;
        
        try {
            destines = (List<Destiny>) em.createNamedQuery("Destiny.findAll").getResultList();
        } catch(Exception e) {
            throw new EJBException(e.getMessage());
        }
        return destines;
    }
    
    public void addDestinyThing(int thingId, int destinyId) {
        Thing thing = em.find(Thing.class, thingId);
        Destiny destiny = em.find(Destiny.class, destinyId);
        thing.addMember(destiny);
    }
}
