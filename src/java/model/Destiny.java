/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author rafal
 */
@Entity
@Table(name = "DESTINY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Destiny.findAll", query = "SELECT d FROM Destiny d"),
    @NamedQuery(name = "Destiny.findById", query = "SELECT d FROM Destiny d WHERE d.id = :id"),
    @NamedQuery(name = "Destiny.findByName", query = "SELECT d FROM Destiny d WHERE d.name = :name")})
public class Destiny implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NAME")
    private String name;
    
    @ManyToMany
    @JoinTable(name="DESTINY_THING", 
        joinColumns={@JoinColumn(name="ID_DESTINY", referencedColumnName = "ID")},
        inverseJoinColumns={@JoinColumn(name="ID_THING", referencedColumnName="ID")}
    )
    private Collection<Thing> things;

    public Destiny() {
    }

    public Destiny(Integer id) {
        this.id = id;
    }

    public Destiny(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Collection<Thing> getThings() { return things; }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Destiny)) {
            return false;
        }
        Destiny other = (Destiny) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Destiny[ id=" + id + " ]";
    }
    
    
    
}
