/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author rafal
 */
@Entity
@Table(name = "THING")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Thing.findAll", query = "SELECT t FROM Thing t"),
    @NamedQuery(name = "Thing.findById", query = "SELECT t FROM Thing t WHERE t.id = :id"),
    @NamedQuery(name = "Thing.findByIdEmployee", query = "SELECT t FROM Thing t WHERE t.idEmployee = :idEmployee"),
    @NamedQuery(name = "Thing.findByName", query = "SELECT t FROM Thing t WHERE t.name = :name")})
public class Thing implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ID_EMPLOYEE")
    private Integer idEmployee;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NAME")
    private String name;
    
    @ManyToOne
    @PrimaryKeyJoinColumn(name = "ID_EMPLOYEE", referencedColumnName = "EMPLOYEE.ID")
    private Employee employee;
    
    @ManyToMany(mappedBy="things")
    private Collection<Destiny> destines;

    
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }
    
    public Thing() {
    }

    public Thing(Integer id) {
        this.id = id;
    }

    public Thing(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Integer idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Thing)) {
            return false;
        }
        Thing other = (Thing) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Thing[ id=" + id + " ]";
    }
    
    public Collection<Destiny> getDestines() {
        return destines;
    }

    public void addMember(Destiny member) {
        destines.add(member);
        member.getThings().add(this);
    }
}
