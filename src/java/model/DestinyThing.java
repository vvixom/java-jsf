/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rafal
 */
@Entity
@Table(name = "DESTINY_THING")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DestinyThing.findAll", query = "SELECT d FROM DestinyThing d"),
    @NamedQuery(name = "DestinyThing.findById", query = "SELECT d FROM DestinyThing d WHERE d.id = :id")})
public class DestinyThing implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "ID_DESTINY", referencedColumnName = "DESTINY.ID")
    @ManyToOne(optional = false)
    private Destiny idDestiny;
    @JoinColumn(name = "ID_THING", referencedColumnName = "THING.ID")
    @ManyToOne(optional = false)
    private Thing idThing;

    public DestinyThing() {
    }

    public DestinyThing(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Destiny getIdDestiny() {
        return idDestiny;
    }

    public void setIdDestiny(Destiny idDestiny) {
        this.idDestiny = idDestiny;
    }

    public Thing getIdThing() {
        return idThing;
    }

    public void setIdThing(Thing idThing) {
        this.idThing = idThing;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DestinyThing)) {
            return false;
        }
        DestinyThing other = (DestinyThing) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.DestinyThing[ id=" + id + " ]";
    }
    
}
